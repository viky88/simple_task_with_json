package com.tutorialspoint.dao;


import com.tutorialspoint.model.User;

import java.util.List;

public interface UserDao {

    User getUserById(int id);

    User getUserByEmail(String email);

    void saveUser(User user);

    void deleteUserById(int id);

    List<User> getAllUsers();

}