package com.tutorialspoint.dao;


import com.tutorialspoint.model.User;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

    public User getUserById(int id_user) {
        return getByKey(id_user);
    }

    @SuppressWarnings("deprecation")
    public User getUserByEmail(String email) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("email", email));
        List<User> us = criteria.list();
        if (us.size()>0) {
            return (User) us.get(0);
        }
        else {
            return null;
        }
    }

    public void saveUser(User user) {
        persist(user);
    }

    public void deleteUserById(int id_user) {
        Query query = getSession().createNativeQuery("DELETE FROM users WHERE id_user = :id_user");
        query.setParameter("id_user", id_user);
        query.executeUpdate();
    }

    public List<User> getAllUsers() {
        CriteriaQuery<User> criteria = criteriaBuilder().createQuery(User.class);
        criteria.from(User.class);
        List<User> users = getSession().createQuery(criteria).getResultList();
//        for (User user : users) {
//            Hibernate.initialize(user.getAmounts());
//        }
        return users;
    }
}
