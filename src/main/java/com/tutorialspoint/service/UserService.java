package com.tutorialspoint.service;


import com.tutorialspoint.model.User;

import java.util.List;

public interface UserService {

    User getUserById(int id);

    User getUserByEmail(String email);

    void createUser(User user);

    void updateUser(User user);

    void deleteUserById(int id);

    List<User> getAllUsers();

}

