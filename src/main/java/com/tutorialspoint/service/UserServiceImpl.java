package com.tutorialspoint.service;


import com.tutorialspoint.dao.UserDao;
import com.tutorialspoint.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao dao;

    public User getUserById(int id) {
        return dao.getUserById(id);
    }

    public User getUserByEmail(String email) {
        return dao.getUserByEmail(email);
    }

    public void createUser(User user) {
        dao.saveUser(user);
    }

    public void updateUser(User user) {
        User entity = dao.getUserById(user.getIdUser());
        if (entity != null) {
            entity.setFirstName(user.getFirstName());
            entity.setLastName(user.getLastName());
            entity.setEmail(user.getEmail());
            entity.setPassword(user.getPassword());
            entity.setRole(user.getRole());
        }
        dao.saveUser(user);
    }

    public void deleteUserById(int id) {
        dao.deleteUserById(id);
    }

    public List<User> getAllUsers() {
        return dao.getAllUsers();
    }

}
