package com.tutorialspoint;

import com.tutorialspoint.model.User;
import com.tutorialspoint.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value="{idUser}", method = RequestMethod.GET)
    public @ResponseBody User getUser(@PathVariable int idUser) {

        User user = userService.getUserById(idUser);

        return user;
    }

//    @RequestMapping(value="{idUser}", method = RequestMethod.POST)
//    public @ResponseBody User updateUser(@PathVariable User user) {
//
//        userService.updateUser(user);
//
//        return user;
//    }
}
